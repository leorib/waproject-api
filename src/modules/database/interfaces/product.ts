export interface IProduct {
  id?: number;
  name: string;
  price: number;
  createdDate?: Date;
  updatedDate?: Date;
}
