import { IUser } from './user';
import { IProduct } from './product';

export interface IOrder {
  id?: number;
  userId?: number;
  productId?: number;
  quantity: number;
  description: string;
  value?: number;

  createdDate?: Date;
  updatedDate?: Date;

  user?: IUser;
  product?: IProduct;
}
