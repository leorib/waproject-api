import * as Knex from 'knex';

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable('Product', table => {
    table.increments('id').primary();

    table.string('name', 400).notNullable();

    table
      .integer('price')
      .unsigned()
      .notNullable();

    table.dateTime('createdDate').notNullable();
    table.dateTime('updatedDate').notNullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTableIfExists('Product');
}
