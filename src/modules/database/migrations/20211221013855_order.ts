import * as Knex from 'knex';

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable('Order', table => {
    table.increments('id').primary();

    table
      .integer('userId')
      .nullable()
      .unsigned()
      .references('id')
      .inTable('User')
      .onDelete('SET NULL');

    table
      .integer('productId')
      .nullable()
      .unsigned()
      .references('id')
      .inTable('Product')
      .onDelete('SET NULL');

    table.string('description', 400).notNullable();

    table
      .integer('value')
      .unsigned()
      .notNullable();

    table
      .integer('quantity')
      .unsigned()
      .notNullable();

    table.dateTime('createdDate').notNullable();
    table.dateTime('updatedDate').notNullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTableIfExists('Order');
}
