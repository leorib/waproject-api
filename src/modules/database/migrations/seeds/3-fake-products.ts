import * as faker from 'faker/locale/pt_BR';
import * as Knex from 'knex';
import { IProduct } from 'modules/database/interfaces/product';
import { IS_DEV } from 'settings';

export async function seed(knex: Knex): Promise<void> {
  if (!IS_DEV) return;

  const products = await knex
    .count()
    .from('Product')
    .first();

  if (Number(products.count) > 0) return;

  for (let x = 0; x < 100; x++) {
    const product: IProduct = {
      name: faker.lorem.word(),
      price: faker.random.number({ min: 100, max: 100000 }),
      createdDate: new Date(),
      updatedDate: new Date()
    };

    await knex.insert(product).into('Product');
  }
}
