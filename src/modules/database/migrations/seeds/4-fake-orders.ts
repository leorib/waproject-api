import * as faker from 'faker/locale/pt_BR';
import * as Knex from 'knex';
import { IOrder } from 'modules/database/interfaces/order';
import { IS_DEV } from 'settings';

export async function seed(knex: Knex): Promise<void> {
  if (!IS_DEV) return;

  const orders = await knex
    .count()
    .from('Order')
    .first();

  const products = await knex.select('id', 'price').from('Product');

  if (Number(orders.count) > 0) return;

  for (let x = 0; x < 100; x++) {
    const { id: productId, price } = products[x];
    const quantity = faker.random.number({ min: 1, max: 50 });
    const value = quantity * price;
    const order: IOrder = {
      userId: faker.random.number({ min: 1, max: 100 }),
      productId,
      quantity,
      value,
      description: faker.lorem.sentences(4),
      createdDate: new Date(),
      updatedDate: new Date()
    };

    await knex.insert(order).into('Order');
  }
}
