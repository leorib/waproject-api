import { ApiProperty } from '@nestjs/swagger';
import { Model } from 'objection';

import { IProduct } from '../interfaces/product';
import { Order } from '../models/order';

export class Product extends Model implements IProduct {
  @ApiProperty({ type: 'integer' })
  public id: number;

  @ApiProperty({ type: 'string' })
  public name: string;

  @ApiProperty({ type: 'integer' })
  public price: number;

  @ApiProperty({ type: 'string', format: 'date-time' })
  public createdDate: Date;

  @ApiProperty({ type: 'string', format: 'date-time' })
  public updatedDate: Date;

  public orders?: Order[];

  public static get tableName(): string {
    return 'Product';
  }

  public static get relationMappings(): any {
    return {
      user: {
        relation: Model.HasManyRelation,
        modelClass: Order,
        join: {
          from: 'Product.id',
          to: 'Order.productId'
        }
      }
    };
  }

  public $beforeInsert(): void {
    this.createdDate = this.updatedDate = new Date();
  }

  public $beforeUpdate(): void {
    this.updatedDate = new Date();
  }
}
