import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { IOrder } from 'modules/database/interfaces/order';
import { Order } from 'modules/database/models/order';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';
import { OrderRepository } from '../repositories/order';
import { ProductRepository } from '../repositories/product';

@Injectable()
export class OrderService {
  constructor(private orderRepository: OrderRepository, private productRepository: ProductRepository) {}
  public async findById(orderId: number, currentUser: ICurrentUser): Promise<Order> {
    const order = await this.orderRepository.findById(orderId);

    if (!order) {
      throw new NotFoundException('product-not-found');
    }

    if (order.userId !== currentUser.id) {
      throw new UnauthorizedException('access-not-allowed');
    }

    return order;
  }

  public async create(model: IOrder, currentUser: ICurrentUser): Promise<Order> {
    const product = await this.productRepository.findById(model.productId);

    if (!product) {
      throw new NotFoundException('product-not-found');
    }

    const value = model.quantity * product.price;

    const order = await this.orderRepository.insert({ ...model, value, userId: currentUser.id });

    return order;
  }
}
