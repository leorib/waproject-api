import { NotFoundException, UnauthorizedException } from '@nestjs/common';
import { OrderRepository } from '../repositories/order';
import { OrderService } from './order';
import { ProductRepository } from '../repositories/product';

import { IOrder } from 'modules/database/interfaces/order';
import { IProduct } from 'modules/database/interfaces/product';

describe('App/OrderService', () => {
  let orderRepository: OrderRepository;
  let productRepository: ProductRepository;
  let service: OrderService;

  const order: IOrder = {
    description: 'Order description',
    quantity: 1
  };

  const product: IProduct = {
    id: 1,
    name: 'Fake product',
    price: 1099
  };

  beforeEach(async () => {
    orderRepository = new OrderRepository();
    productRepository = new ProductRepository();
    service = new OrderService(orderRepository, productRepository);
  });

  it('should be able to find an order', async () => {
    jest.spyOn(orderRepository, 'findById').mockResolvedValueOnce({ ...order, userId: 1 } as any);

    const result = await service.findById(1, { id: 1 } as any);

    expect(result).not.toBeFalsy();
    expect(result).toEqual({ ...order, userId: 1 });
  });

  it('should throw Unauthorized exception if the user is not the order creator', async () => {
    jest.spyOn(orderRepository, 'findById').mockResolvedValueOnce({ ...order, userId: 1 } as any);

    try {
      await service.findById(1, { id: 2 } as any);

      fail();
    } catch (err) {
      expect(err).toBeInstanceOf(UnauthorizedException);
    }
  });

  it('should be able to create an order', async () => {
    jest.spyOn(productRepository, 'findById').mockResolvedValueOnce({ ...product } as any);
    jest.spyOn(orderRepository, 'insert').mockImplementationOnce(order => Promise.resolve({ ...order } as any));

    const result = await service.create(order, { id: 1 } as any);

    expect(result).not.toBeFalsy();
    expect(result).toEqual({ ...order, userId: 1, value: order.quantity * product.price });
  });

  it('should correctly calculate order value when creating', async () => {
    const quantity = 2;
    const expectedValue = product.price * quantity;
    jest.spyOn(productRepository, 'findById').mockResolvedValueOnce({ ...product } as any);
    jest.spyOn(orderRepository, 'insert').mockImplementationOnce(({ productId, value }) => {
      expect(productId).toBe(product.id);
      expect(value).toBe(expectedValue);
      return Promise.resolve(null);
    });
  });

  it('should throw NotFoundException when trying to create an order with a non existent product', async () => {
    jest.spyOn(productRepository, 'findById').mockResolvedValueOnce(null);

    try {
      await service.create({ order } as any, { id: 1 } as any);

      fail();
    } catch (err) {
      expect(err).toBeInstanceOf(NotFoundException);
    }
  });
});
