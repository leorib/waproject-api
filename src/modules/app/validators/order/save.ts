import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsString, Max, MaxLength, Min, MinLength } from 'class-validator';
import { IOrder } from 'modules/database/interfaces/order';

export class SaveValidator implements IOrder {
  @IsNotEmpty()
  @IsInt()
  @Min(0)
  @ApiProperty({ required: true, type: 'integer', example: 1 })
  public productId: number;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(200)
  @ApiProperty({ required: true, type: 'string', minLength: 3, maxLength: 200, example: 'Order description' })
  public description: string;

  @IsNotEmpty()
  @IsInt()
  @Min(1)
  @Max(10000)
  @ApiProperty({ required: true, type: 'integer', minimum: 1, maximum: 10000, example: 1 })
  public quantity: number;
}
