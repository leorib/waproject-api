import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthRequired } from 'modules/common/guards/token';
import { enRoles } from 'modules/database/interfaces/user';
import { Product } from 'modules/database/models/product';

import { ProductRepository } from '../repositories/product';
import { ListValidator } from '../validators/product/list';

@ApiTags('App: Product')
@Controller('/product')
@AuthRequired([enRoles.sysAdmin, enRoles.admin, enRoles.user])
export class ProductController {
  constructor(private productRepository: ProductRepository) {}

  @Get()
  @ApiResponse({ status: 200, type: [Product] })
  public async list(@Query() model: ListValidator) {
    return this.productRepository.list(model);
  }

  @Get(':productId')
  @ApiResponse({ status: 200, type: Product })
  public async details(@Param('productId', ParseIntPipe) productId: number) {
    return this.productRepository.findById(productId);
  }
}
