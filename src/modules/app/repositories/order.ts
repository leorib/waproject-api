import { Injectable } from '@nestjs/common';
import { Page, Transaction } from 'objection';
import { IOrder } from 'modules/database/interfaces/order';
import { Order } from 'modules/database/models/order';
import { IPaginationParams } from 'modules/common/interfaces/pagination';

@Injectable()
export class OrderRepository {
  public async findById(id: number, transaction?: Transaction): Promise<Order> {
    return Order.query(transaction).findById(id);
  }

  public async list(params: IPaginationParams, userId: number, transaction?: Transaction): Promise<Page<Order>> {
    let query = Order.query(transaction)
      .select('*')
      .where('userId', userId)
      .page(params.page, params.pageSize);

    if (params.orderBy) {
      query = query.orderBy(params.orderBy, params.orderDirection);
    }

    if (params.term) {
      query = query.where(query => {
        return query.where('description', 'ilike', `%${params.term}%`);
      });
    }

    return query;
  }

  public async insert(model: IOrder, transaction: Transaction = null): Promise<Order> {
    return Order.query(transaction).insertAndFetch(model as any);
  }

  public async update(model: IOrder, transaction: Transaction = null): Promise<Order> {
    return Order.query(transaction).patchAndFetchById(model.id, model as any);
  }

  public async remove(orderId: string, transaction: Transaction = null): Promise<void> {
    await Order.query(transaction).deleteById(orderId);
  }
}
